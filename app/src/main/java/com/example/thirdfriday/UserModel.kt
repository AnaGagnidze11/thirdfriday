package com.example.thirdfriday

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class UserModel(var name: String, var lastName: String, var email: String): Parcelable
package com.example.thirdfriday

interface UserItemListener {
   fun updateButtonOnClick(position: Int)
   fun deleteButtonOnClick(position: Int)

}
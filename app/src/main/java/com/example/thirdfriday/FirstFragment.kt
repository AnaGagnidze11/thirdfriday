package com.example.thirdfriday

import android.os.Bundle
import android.util.Log.i
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thirdfriday.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding: FragmentFirstBinding

    private val users = mutableListOf<UserModel>()

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstBinding.inflate(inflater, container, false)
        addUsers()
        init()

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val position = arguments?.getInt("position", -1)
        i("Pinpoint", "$position")
        val user = arguments?.getParcelable<UserModel>("user")
        if (position == -1 && user != null){
            users.add(user)
            adapter.notifyItemInserted(users.size - 1)
        }else if (position != -1 && user != null){
            users[position!!] = user
            adapter.notifyItemChanged(position)

        }
    }


    private fun init() {
        adapter = RecyclerViewAdapter(users, object : UserItemListener {
            override fun updateButtonOnClick(position: Int) {
                val bundle = bundleOf("user" to users[position], "position" to position)
                findNavController().navigate(R.id.action_firstFragment_to_secondFragment, bundle)
            }

            override fun deleteButtonOnClick(position: Int) {
                users.removeAt(position)
                adapter.notifyItemRemoved(position)
            }

        })

        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter

        binding.ImBAddUser.setOnClickListener {
            findNavController().navigate(R.id.action_firstFragment_to_secondFragment)
        }

    }



    private fun addUsers() {
        users.add(UserModel("Ana", "Gagnidze", "ananogagnidze@gmail.com"))
        users.add(UserModel("Dylan", "O'Brien", "dylan@gmail.com"))
        users.add(UserModel("Harry", "Styles", "harry@gmail.com"))
        users.add(UserModel("Toby", "The cat", "toby@gmail.com"))
    }

}
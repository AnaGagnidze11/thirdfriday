package com.example.thirdfriday

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.thirdfriday.databinding.UserItemLayoutBinding

class RecyclerViewAdapter(
    private val users: MutableList<UserModel>,
    private val userItemListener: UserItemListener
) : RecyclerView.Adapter<RecyclerViewAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemView =
            UserItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = UserViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = users.size

    inner class UserViewHolder(private val binding: UserItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var user: UserModel
        fun bind() {
            user = users[adapterPosition]
            binding.TxVName.text = user.name
            binding.TxVLastName.text = user.lastName
            binding.TxVEmail.text = user.email
            binding.ImBUpdateUser.setOnClickListener(this)
            binding.ImBDeleteUser.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v ?: itemId) {
                binding.ImBUpdateUser -> userItemListener.updateButtonOnClick(adapterPosition)
                binding.ImBDeleteUser -> userItemListener.deleteButtonOnClick(adapterPosition)
            }
        }
    }
}
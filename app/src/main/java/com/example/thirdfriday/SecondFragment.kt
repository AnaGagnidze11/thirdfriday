package com.example.thirdfriday

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.thirdfriday.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {

    private lateinit var binding: FragmentSecondBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        binding.saveButton.setOnClickListener{
            newInfo()
        }
    }

    private fun newInfo(){
        val name = binding.firstNameEdtT.text.toString()
        val lastName = binding.lastNameEdtT.text.toString()
        val email = binding.emailEdT.text.toString()

        if (name.isNotEmpty() && lastName.isNotEmpty() && email.isNotEmpty()){
            val user = UserModel(name, lastName, email)
            val bundle = bundleOf("user" to user, "position" to arguments?.getInt("position", -1))
            findNavController().navigate(R.id.action_secondFragment_to_firstFragment, bundle)
        }
    }



}